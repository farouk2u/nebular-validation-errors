import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NbErrorTooltipDirective} from './directives/nb-error-tooltip.directive';
import {NbAdjustment, NbPosition} from '@nebular/theme';
import {NB_ERROR_TOOLTIP_CONFIG} from './tokens';

export interface NbErrorTooltipModuleConfig {
  position?: NbPosition;
  adjustment?: NbAdjustment;
}


@NgModule({
  declarations: [NbErrorTooltipDirective],
  exports: [
    NbErrorTooltipDirective
  ],
  imports: [
    CommonModule
  ],
  providers: [
    {
      provide: NB_ERROR_TOOLTIP_CONFIG, useFactory: getConfig
    } // Default configuration
  ]
})
export class NbErrorTooltipModule {

  static withConfig(config?: NbErrorTooltipModuleConfig) {
    return {
      ngModule: NbErrorTooltipModule,
      providers: [
        {provide: NB_ERROR_TOOLTIP_CONFIG, useValue: config}
      ]
    } as ModuleWithProviders<NbErrorTooltipModule>;
  }
}


export function getConfig(){
  return {};
}

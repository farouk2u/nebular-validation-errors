import {
  Directive,
  ElementRef,
  Inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Optional,
  Self,
  SimpleChanges
} from '@angular/core';
import {NgControl} from '@angular/forms';
import {fromEvent, merge, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {
  NbAdjustment,
  NbComponentStatus,
  NbDynamicOverlay,
  NbDynamicOverlayHandler,
  NbIconConfig,
  NbInputDirective,
  NbPosition,
  NbTooltipComponent,
  NbTrigger
} from '@nebular/theme';
import {
  NB_ERROR_NORMALIZER,
  NbErrorNormalizerInterface,
  NbErrorNormalizerResult
} from '../../../interfaces/nb-error-normalizer.interface';
import {NbErrorTooltipModuleConfig} from '../nb-error-tooltip.module';
import {NB_ERROR_TOOLTIP_CONFIG} from '../tokens';

@Directive({
  selector: '[nbErrorTooltip]',
  providers: [NbDynamicOverlayHandler, NbDynamicOverlay]
})
export class NbErrorTooltipDirective implements OnChanges, OnInit, OnDestroy {

  private static readonly DEFAULT_CONTENT = 'This field is invalid';
  @Input('nbErrorTooltipPlacement')
  position: NbPosition = NbPosition.TOP;
  @Input('nbErrorTooltipAdjustment')
  adjustment: NbAdjustment = NbAdjustment.CLOCKWISE;
  @Input('icon')
  icon: string | NbIconConfig = null;
  destroy$: Subject<boolean> = new Subject<boolean>();

  nbComponentOriginalStatus: NbComponentStatus;
  private dynamicOverlay: NbDynamicOverlay;
  private tooltipComponent = NbTooltipComponent;
  private content = 'This field is invalid';

  constructor(
    @Self() @Optional() private control: NgControl,
    @Self() @Optional() private nbInput: NbInputDirective,
    @Inject(NB_ERROR_TOOLTIP_CONFIG) private config: NbErrorTooltipModuleConfig,
    @Optional() @Inject(NB_ERROR_NORMALIZER) private errorNormalizer: NbErrorNormalizerInterface,
    private hostRef: ElementRef<any>,
    private dynamicOverlayHandler: NbDynamicOverlayHandler,
  ) {
    // Override default values if present in config
    this.position = this.config.position || this.position;
    this.adjustment = this.config.adjustment || this.adjustment;
  }

  get isShown(): boolean {
    return !!(this.dynamicOverlay && this.dynamicOverlay.isAttached);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.control) {
      this.rebuild();
    }
  }

  ngOnInit() {

    if (!this.control) {
      console.warn("nbErrorTooltip: this element is not an NgControl", this.hostRef);
      return;
    }

    this.dynamicOverlayHandler
      .host(this.hostRef)
      .componentType(this.tooltipComponent)
      .offset(8);

    merge(
      this.control.valueChanges,
      fromEvent(this.hostRef.nativeElement, 'blur')
      // .pipe(tap(x => console.log('Captured blur: ', x)))
    ).pipe(
      takeUntil(this.destroy$)
    ).subscribe((data) => {
      if (this.control.dirty || this.control.touched) {
        const errors = this.control.errors;

        if (errors) {
          if (this.errorNormalizer) {
            const res = this.errorNormalizer.normalize(errors) || {} as NbErrorNormalizerResult;
            this.content = res.message;
          }
          this.content = this.content || NbErrorTooltipDirective.DEFAULT_CONTENT;
          this.setError(true);
        } else {
          this.setError(false);
        }
      } else {
        this.setError(false);
      }
    });
  }

  rebuild() {
    this.dynamicOverlay = this.configureDynamicOverlay()
      .rebuild();
  }

  ngOnDestroy(): void {
    this.dynamicOverlayHandler.destroy();
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  ngAfterViewInit(): void {
    if (this.control) {
      this.dynamicOverlay = this.configureDynamicOverlay()
        .build();
    }
  }

  show() {
    this.dynamicOverlay.show();
  }

  hide() {
    this.dynamicOverlay.hide();
  }

  toggle() {
    this.dynamicOverlay.toggle();
  }

  protected configureDynamicOverlay() {
    return this.dynamicOverlayHandler
      .position(this.position)
      .trigger(NbTrigger.NOOP)
      .adjustment(this.adjustment)
      .content(this.content)
      .context({status: 'danger' as NbComponentStatus, icon: this.icon})
      .overlayConfig({});
  }

  private setError(error: boolean) {
    if (error) {
      if (this.nbInput) {
        this.nbComponentOriginalStatus = this.nbComponentOriginalStatus || this.nbInput.status; // Write only if null
        this.nbInput.status = 'danger';
      }
      this.rebuild();
      this.show();
    } else {
      if (this.isShown) {
        this.hide();
      }
      if (this.nbInput && this.nbComponentOriginalStatus) {
        this.nbInput.status = this.nbComponentOriginalStatus;
        this.nbComponentOriginalStatus = null;
      }
    }
  }

}

import {InjectionToken} from '@angular/core';

export const NB_ERROR_NORMALIZER = new InjectionToken<NbErrorNormalizerInterface>('NB_ERROR_NORMALIZER');

export interface NbErrorNormalizerResult {
  message: string;
}

export interface NbErrorNormalizerInterface {
  normalize(errors: any): NbErrorNormalizerResult;
}

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LazyPage} from './pages/lazy/lazy.page';


const routes: Routes = [
  {
    path: '',
    component: LazyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LazyRoutingModule {
}

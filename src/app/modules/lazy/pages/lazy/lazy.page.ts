import { Component, OnInit } from '@angular/core';
import {FormControl, NgForm} from '@angular/forms';
import { NbPosition } from '@nebular/theme';

@Component({
  selector: 'app-lazy',
  templateUrl: './lazy.page.html',
  styleUrls: ['./lazy.page.scss']
})
export class LazyPage implements OnInit {

  NbPosition = NbPosition;
  title = 'nebular-validation-errors';
  formData: any = {};

  markAsDirty(form: NgForm){
    for (const c in form.controls) {
      const control = form.controls[c] as FormControl;
      control.markAsDirty();
      control.markAsTouched();
      // To trigger change
      control.setValue(control.value);
    }
  }

  markAsPristine(form: NgForm){
    for (const c in form.controls) {
      const control = form.controls[c] as FormControl;
      control.markAsPristine();
      control.markAsUntouched();
      control.setValue(undefined);
    }
  }

  ngOnInit(): void {
  }

}

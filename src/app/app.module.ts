import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {
  NbButtonModule,
  NbCardModule,
  NbInputModule,
  NbLayoutModule,
  NbPosition,
  NbSidebarModule,
  NbThemeModule
} from '@nebular/theme';
import {FormsModule} from '@angular/forms';
import {NB_ERROR_NORMALIZER, NbErrorTooltipModule} from '@sungazer/nebular-validation-errors';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ErrorNormalizerService} from './services/error-normalizer.service';
import {FormPage} from './pages/form/form.page';

@NgModule({
  declarations: [
    AppComponent,
    FormPage
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NbThemeModule.forRoot({name: 'default'}),
    NbLayoutModule,
    FormsModule,
    NbSidebarModule.forRoot(),
    NbCardModule,
    NbInputModule,
    NbErrorTooltipModule,
    BrowserAnimationsModule,
    NbButtonModule,
  ],
  providers: [
    {provide: NB_ERROR_NORMALIZER, useExisting: ErrorNormalizerService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

import {Component, OnInit} from '@angular/core';
import {FormControl, NgForm} from '@angular/forms';
import {NbPosition} from '@nebular/theme';

@Component({
  selector: 'app-form',
  templateUrl: './form.page.html',
  styleUrls: ['./form.page.scss']
})
export class FormPage implements OnInit {

  NbPosition = NbPosition;
  formData: any = {};

  markAsDirty(form: NgForm) {
    for (const c in form.controls) {
      const control = form.controls[c] as FormControl;
      control.markAsDirty();
      control.markAsTouched();
      // To trigger change
      control.setValue(control.value);
    }
  }

  markAsPristine(form: NgForm) {
    for (const c in form.controls) {
      const control = form.controls[c] as FormControl;
      control.markAsPristine();
      control.markAsUntouched();
      control.setValue(undefined);
    }
  }

  ngOnInit(): void {
  }

}

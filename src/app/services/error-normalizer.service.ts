import {Injectable} from '@angular/core';
import {NbErrorNormalizerInterface, NbErrorNormalizerResult} from '@sungazer/nebular-validation-errors';

@Injectable({
  providedIn: 'root'
})
export class ErrorNormalizerService implements NbErrorNormalizerInterface {

  constructor() {
  }

  normalize(errors: any): NbErrorNormalizerResult {
    if (errors.required) {
      return {message: 'This field is required'};
    }
    return null;
  }
}

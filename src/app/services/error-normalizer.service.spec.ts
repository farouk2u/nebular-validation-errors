import { TestBed } from '@angular/core/testing';

import { ErrorNormalizerService } from './error-normalizer.service';

describe('ErrorNormalizerService', () => {
  let service: ErrorNormalizerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ErrorNormalizerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

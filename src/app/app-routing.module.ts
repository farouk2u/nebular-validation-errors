import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppComponent} from './app.component';
import {FormPage} from './pages/form/form.page';


const routes: Routes = [
  {
    path: 'form',
    component: FormPage,
  },
  {
    path: 'lazy',
    loadChildren: () => import('./modules/lazy/lazy.module').then(m => m.LazyModule),
  },
  {
    path: '',
    redirectTo: 'form',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

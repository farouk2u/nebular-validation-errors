# NebularValidationErrors

Aim of this repository is to automate validation error presentation in forms built with Nebular (https://akveo.github.io/nebular/).

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
